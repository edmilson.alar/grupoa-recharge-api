CREATE TABLE billionaires (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  first_name VARCHAR(250) NOT NULL,
  last_name VARCHAR(250) NOT NULL,
  career VARCHAR(250) DEFAULT NULL
);

--Insert into CountryCode (countryCode,prefix,digitsNumber)
--Values ('MZ','+258',9),('RSA','+277',12),('USA','+01',9)