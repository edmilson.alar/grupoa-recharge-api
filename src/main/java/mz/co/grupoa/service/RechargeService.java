package mz.co.grupoa.service;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import mz.co.grupoa.interfaces.ICountryCode;
import mz.co.grupoa.interfaces.IToken;
import mz.co.grupoa.interfaces.ITransaction;
import mz.co.grupoa.model.CountryCode;
import mz.co.grupoa.model.Token;
import mz.co.grupoa.model.Transaction;

@Service
public class RechargeService {

	@Autowired
	IToken iToken;
	
	@Autowired
	ITransaction iTransaction;
	
	@Autowired
	ICountryCode iCountryCode;
	
	@Autowired
	private Environment env;
	
	public String generateToken(int amount) {
		
		TripleDes tripleDES = new TripleDes(env.getProperty("encrypt.key"));
		
		int randomNumber=0;
		
		String dateTime="";
		
		String codeToEncrypt="";
		
		String encryptedCode="";
		
		Token token = new Token();
		
		Random random = new Random();
		
		randomNumber = random.nextInt(1000);
		
		dateTime = LocalDateTime.now().toString();
		
		codeToEncrypt = codeToEncrypt.concat(randomNumber+"").concat(dateTime).concat(amount+"");
		
		try {
			encryptedCode = tripleDES.encrypt(codeToEncrypt);
		
		} catch (InvalidKeyException | NoSuchAlgorithmException | UnsupportedEncodingException | NoSuchPaddingException
				| IllegalBlockSizeException | BadPaddingException e) {
			
			e.printStackTrace();
		}
		
		token.setToken(encryptedCode);
		token.setAmount(amount);
		
		iToken.save(token);
		
		return encryptedCode;
	}
	
	public boolean validateRecharge(String tokenToValidate) {
		
		TripleDes tripleDES = new TripleDes(env.getProperty("encrypt.key"));
				
		String decryptedToken="";
		
		Token token = iToken.findByToken(tokenToValidate);
		if(token==null) {
			return false;
		}
		Transaction transaction = iTransaction.findByToken(token);
		
		if(transaction != null) {
			return false; 
		}
		
		try {
			
			decryptedToken = tripleDES.decrypt(tokenToValidate,env.getProperty("encrypt.key"));
		
		} catch (InvalidKeyException | UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException
				| IllegalBlockSizeException | BadPaddingException e) {
			e.printStackTrace();
		}
		
		if(decryptedToken.length()!=Integer.parseInt(env.getProperty("recharge.size"))){
			return false;
		}
		
		return true;
	}
	
	/*
	 * Validar o tamanho do numero pelo codigo do País
	 * */
	public boolean validateNumber(String countryCode, String number ) {
		
		return true;
	}
	
	public boolean recharge(String encryptedCode, String countryCode,String cellPhoneNumber) {
		TripleDes tripleDES = new TripleDes(env.getProperty("encrypt.key"));
		
		if(validateRecharge(encryptedCode) && validateNumber(countryCode, cellPhoneNumber)) {
		Token token = iToken.findByToken(encryptedCode);
		Transaction transaction = new Transaction();
		int amount=0;
		
		String decodedCode;
		try {
			decodedCode = tripleDES.decrypt(encryptedCode, env.getProperty("encrypt.key"));
	
			amount = Integer.parseInt(decodedCode.substring(24));
			
			transaction.setAmount(amount);
			transaction.setCellPhoneNumber(cellPhoneNumber);
			transaction.setToken(token);
			transaction.setDatetime(LocalDateTime.now());
			
				return true;
		
		} catch (InvalidKeyException | UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException
				| IllegalBlockSizeException | BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		}
		return false;
	}
	
	
	public List<CountryCode> getCountries(){
		return iCountryCode.findAll();
	}
	
	public List<Token> getTokens(){
		return iToken.findAll();
	}
	
	public void saveCountry(CountryCode country) {
		iCountryCode.save(country)	;	
	}
}
