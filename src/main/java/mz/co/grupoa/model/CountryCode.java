package mz.co.grupoa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CountryCode {

	@Id	
	@Column(unique=true)
	private String countryCode;
	
	private String countryName;

	@Column(unique=true)
	private String prefix;
	
	private int digitsNumber;

	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public int getDigitsNumber() {
		return digitsNumber;
	}
	public void setDigitsNumber(int digitsNumber) {
		this.digitsNumber = digitsNumber;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	
	
}
