package mz.co.grupoa.api;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import mz.co.grupoa.bean.PromotionBean;
import mz.co.grupoa.bean.TransactionBean;
import mz.co.grupoa.service.RechargeService;

@RestController
@RequestMapping("/recharge-api")
public class RechargeApi {
	
	@Autowired
	RechargeService rechargeService;
	
	@CrossOrigin
	@GetMapping("/totaltransactions")
	public @ResponseBody ResponseEntity<?> getTransactionsCount() {
		return ResponseEntity.status(200).body(150);
		
	}
	
	
	@CrossOrigin
	@GetMapping("/totaltransactionsamount")
	public @ResponseBody ResponseEntity<?> getADriversLocation() {
		return ResponseEntity.status(200).body(new ArrayList<TransactionBean>());
		
	}
	
	@CrossOrigin
	@RequestMapping(value="/generatetoken/{amount}")
	public @ResponseBody ResponseEntity<?> generatetoken(@PathVariable int amount) {
          
		String token =  rechargeService.generateToken(amount);
        return new ResponseEntity<>(token, HttpStatus.OK);
	}
	
	@CrossOrigin
	@RequestMapping("/recharge/{countryCode}/{mobileNumber}/{token}")
	public @ResponseBody ResponseEntity<?> recharge(@PathVariable String countryCode, @PathVariable String mobileNumber, @PathVariable String token) {
            
		boolean recharged=false ;
        return new ResponseEntity<>(recharged, HttpStatus.OK);
	}
	
	@CrossOrigin
	@PostMapping("/setpromotion")
	public @ResponseBody ResponseEntity<?> setPromotion(@RequestBody PromotionBean promotionBean) {
             
		boolean recharged=false ;
        return new ResponseEntity<>(recharged, HttpStatus.OK);
	}

	
	
	@CrossOrigin
	@RequestMapping(value="/gettokens")
	public @ResponseBody ResponseEntity<?> getTokens() {
	
		return new ResponseEntity<>(rechargeService.getTokens(), HttpStatus.OK);
	}
	
	@CrossOrigin
	@RequestMapping(value="/getcountries")
	public @ResponseBody ResponseEntity<?> getCountries() {
	
		return new ResponseEntity<>(rechargeService.getCountries(), HttpStatus.OK);
	}
}
