package mz.co.grupoa.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mz.co.grupoa.model.Token;
import mz.co.grupoa.model.Transaction;

@Repository
public interface ITransaction extends JpaRepository<Transaction, Long>{
	
	public Transaction findByToken(Token token);
	
	
}
