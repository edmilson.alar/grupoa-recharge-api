package mz.co.grupoa.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mz.co.grupoa.model.CountryCode;

@Repository
public interface ICountryCode extends JpaRepository<CountryCode, String>{
	
	public CountryCode findByCountryCode(String countryCode);
}
