package mz.co.grupoa.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mz.co.grupoa.model.Token;

@Repository
public interface IToken extends JpaRepository<Token, Long>{
	
	public Token findByToken(String token);
}
